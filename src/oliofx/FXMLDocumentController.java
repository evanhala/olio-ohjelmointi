/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import oliokurssi.Doge;
import oliokurssi.DogeUniversity;

/**
 *
 * @author evanhala
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Button clearButton;
    @FXML
    private TextField inputField;
    @FXML
    private Slider sizeSlider;
    @FXML
    private Button speakButton;
    @FXML
    private Button addButton;
    @FXML
    private TextField nameField;
    @FXML
    private TextField ageField;
    @FXML
    private TextField colorField;
    @FXML
    private ComboBox<Doge> combo;
    @FXML
    private TextArea textOutputArea;
    @FXML
    private TextField speakInputText;
    @FXML
    private Button dogeCounterButton;
    @FXML
    private Button urlButton;
    @FXML
    private TextArea netOutputArea;
    @FXML
    private TextField urlInput;
    @FXML
    private Label weatherLabel;
    @FXML
    private Button webviewButton;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Text added");
        label.setText(label.getText() + inputField.getText());
        inputField.clear();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        combo.getItems().add(new Doge(13, "musta", "Leevi"));
        /*DogeUniversity du = null;
        DogeUniversity du2 = null;
        du = DogeUniversity.getInstance();

        
        du.addDoge(new Doge(2, "musta", "Leevi"));
        du.addDoge(new Doge(3, "harmaa", "Lassi"));
        
        du.cry();
        
        
        du2 = DogeUniversity.getInstance();
        
        du2.cry();*/
        
        /*Pair p1 = new Pair(2, new Doge());
        Pair p2 = new Pair(3.33, "foobar");
        
        p1.printValues();
        p2.printValues();
        
        
        ArrayList<Doge> al = new ArrayList<>();
        al.add(new Doge());
        al.add(new Doge());
        al.add(new Doge());
        
        for(Doge d : al) {
            System.out.println(d.speak());
        }
        
        Iterator itr = al.iterator();
        while(itr.hasNext()) {
            System.out.println(((Doge)itr.next()).speak());
        }
        
        ListIterator litr = al.listIterator(al.size());
        while(litr.hasPrevious()) {
            System.out.println(((Doge)litr.previous()).speak());
        }*/
        
    }    

    @FXML
    private void clearAction(ActionEvent event) {
        label.setText("");
    }
    
    @FXML
    private void sliderChange(MouseEvent event) {
        System.out.println("Slider changed");
        label.setFont(new Font(sizeSlider.getValue()));
    }

    @FXML
    private void speakAction(ActionEvent event) {
        
        try {
            textOutputArea.setText(textOutputArea.getText()
                    + combo.valueProperty().getValue().speak(speakInputText.getText()) + "\n" );
        } catch (SpeakException ex) {
            System.err.println(ex.getMessage());
            System.err.println("Message was a only " + ex.getLenght() + " chars.");
            textOutputArea.setText(textOutputArea.getText() + "<<" + ex.getMessage() + ">>\n");
        }
        
        
        System.out.println(combo.valueProperty().getValue().speak());
    }

    @FXML
    private void addDogeAction(ActionEvent event) {
        
        Doge d = new Doge((int)Double.parseDouble(ageField.getText()), colorField.getText(), nameField.getText());
        
        combo.getItems().add(d);
        
    }

    @FXML
    private void printDogeCounter(ActionEvent event) {
        System.out.println(Doge.getDogeCounter());
    }

    @FXML
    private void getUrlAction(ActionEvent event) throws MalformedURLException, IOException {
        URL url = new URL(urlInput.getText());
        
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
        String content = "";
        String line;
        
        while((line = br.readLine()) != null) {
            content += line + "\n";
        }
        
        netOutputArea.setText(content);
        
        Weather w = new Weather(content);
        
        weatherLabel.setText((String)w.getMap().get("weather") + "\n"
                            + (String)w.getMap().get("temperature") + "\n"
                            + (String)w.getMap().get("humidity") + "\n"
                            + (String)w.getMap().get("wind") + "\n"
                            + (String)w.getMap().get("clouds") + "\n"
        
        );
        
        
        
    }

    @FXML
    private void startWebview(ActionEvent event) {
        
        try {
            Stage webview = new Stage();
            
            Parent page = FXMLLoader.load(getClass().getResource("FXMLWebView.fxml"));
            
            Scene scene = new Scene(page);
            
            webview.setScene(scene);
            webview.show();
            
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    
}
