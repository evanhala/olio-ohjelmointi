/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author evanhala
 */
public class FXMLWebViewController implements Initializable {
    @FXML
    private Button loadButton;
    @FXML
    private Button drawButton;
    @FXML
    private WebView web;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load("http://www.lut.fi");
    }    

    @FXML
    private void loadLocalFile(ActionEvent event) {
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }

    @FXML
    private void drawRoad(ActionEvent event) {
        web.getEngine().executeScript("document.goToLocation('Skinnarilankatu 34, 53850 Lappeenranta', 'Auki: 10-18', 'blue')");
        
        ArrayList<Float> al = new ArrayList<>();
        
        al.add(61.0543675f);
        al.add(28.1832322f);
        al.add(61.498112f);
        al.add(23.7660095f);
        
        web.getEngine().executeScript("document.createPath("+ al + ", 'red', 1)");
        
        
    }
    
}
