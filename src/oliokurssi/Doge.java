/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliokurssi;

/**
 *
 * @author evanhala
 */
public class Doge extends Animal implements Amaze, java.io.Serializable {
    
    static private int dogeCounter = 0;

    public Doge() {
        super(1, "pink", "Doge");
        dogeCounter++;
        System.out.println("Such wow");
    }
    
    public Doge(int a, String c, String n) {
        super(a, c, n);
        dogeCounter++;
        System.out.println("Doge has amazed, such wow");
    }
    
    @Override
    public String speak() {
        return "Much speak, such happy";
    }
    
    @Override
    public String wow(){
        return "Such " + color + ", " + age + " years, wow";
    
    }
    
    public static int getDogeCounter() {
        return dogeCounter;
    }    
    
}
