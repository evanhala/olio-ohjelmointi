/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliokurssi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author evanhala
 */
public class IOoperator {
    private String filename;
    
    public IOoperator(String s) {
        filename = s;
    }
    
    public void writeText(String text) throws FileNotFoundException, IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));
        out.write(text);
        out.close();
    
    }
    public String readText() throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(filename));
        String s = in.readLine();
        in.close();
        return s;
    }
    
    public void saveDogeUniversity(DogeUniversity du) throws FileNotFoundException, IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("doges.ser"));
        out.writeObject(du);
        out.close();
    
    }
    
    public DogeUniversity loadDogeUniversity() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("doges.ser"));
        
        DogeUniversity du = (DogeUniversity) in.readObject();
    
        return du;
    
    }
    
    
}
